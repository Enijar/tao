<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>TAO</title>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <script>
            function url(path) {
                path = typeof(path) !== 'undefined' ? path : '/';
                path = path.replace(/^\/+/, '');
                return '{{ url('/') }}/' + path;
            }

            function asset(path) {
                path = typeof(path) !== 'undefined' ? path : '/';
                return url(path);
            }

            window.data = {
                player: {
                    username: 'User A',
                    stats: 750,
                    type: 'grey'
                }
            };
        </script>
    </head>

    <body>
        <div id="app"></div>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
