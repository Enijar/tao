import React, {Component} from "react";
import config from "../config";
import TAO from "../TAO";
import UnitStats from "./UnitStats";
import ActionButtons from "./ActionButtons";
import SettingButtons from "./SettingButtons";
import Login from "./Login";
import Chat from "./Chat";
import Container from "../TAO/Container";

class Game extends Component {
    constructor(props) {
        super(props);

        this.canvas = null;
        this.ctx = null;
        this.TAO = null;
        this.state = {
            game: Container.game
        };
        this.subscriptions = [];

        this.updateGame = this.updateGame.bind(this);
    }

    componentDidMount() {
        this.renderGame();
        this.subscriptions.push(Container.Event.addListener('game.update', this.updateGame));
    }

    componentWillUnmount() {
        this.subscriptions.map(subscription => subscription.remove());
    }

    renderGame() {
        if (Container.game.state !== 'login') {
            this.canvas = this.refs.canvas;
            this.ctx = this.canvas.getContext('2d');
            this.TAO = new TAO({canvas: this.canvas, ctx: this.ctx});
            this.TAO.render();
        }
    }

    updateGame() {
        this.setState({game: Container.game}, () => {
            this.renderGame();
        });
    }

    getView() {
        if (this.state.game.state === 'login') {
            return <Login/>;
        }

        return (
            <canvas
                ref="canvas"
                width={config.game.width}
                height={config.game.height}
            />
        );
    }

    render() {
        return (
            <div id="game" style={{maxWidth: `${config.game.width}px`}}>
                <UnitStats game={this.state.game}/>
                <ActionButtons game={this.state.game}/>
                <SettingButtons game={this.state.game}/>

                {this.getView()}

                <Chat game={this.state.game}/>
            </div>
        );
    }
}

export default Game;
