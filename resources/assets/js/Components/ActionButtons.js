import React, {Component} from "react";
import PropTypes from "prop-types";
import Container from "../TAO/Container";

class ActionButtons extends Component {
    constructor(props) {
        super(props);

        this.states = [
            'move',
            'attack',
            'turn',
            'pass',
            'surrender',
        ];

        this.state = {
            state: null,
        };
    }

    gameStateToggle(state) {
        this.setState({state});
        Container.Event.emit('game.state.change', state);
    }

    render() {
        if (this.props.game.state !== 'arena') {
            return null;
        }

        return (
            <div className="ActionButtons">
                {this.states.map((state, index) => (
                    <div
                        key={index}
                        className={`
                            ActionButtons__button
                            ActionButtons__button--${state}
                            ${this.state.state === state ? 'selected' : ''}
                        `}
                        onClick={() => this.gameStateToggle(state)}
                    >
                        <img
                            src={asset(`/img/${state}.png`)}
                            onDragStart={event => event.preventDefault()}
                        />
                    </div>
                ))}
            </div>
        );
    }
}

ActionButtons.propTypes = {
    game: PropTypes.object.isRequired,
};

export default ActionButtons;
