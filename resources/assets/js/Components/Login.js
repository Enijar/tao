import React, {Component} from "react";
import Container from "../TAO/Container";

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            password: '',
            password_confirm: '',
            newAccount: false,
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        this.login();
    }

    toggleNewAccount() {
        this.setState({newAccount: !this.state.newAccount});
    }

    login() {
        // TODO: Handle response from server.
        console.log('Login.login', this.state);
        Container.game.state = 'lobby';
        Container.Event.emit('game.update');
    }

    render() {
        return (
            <div className="Login">
                <br/>
                <br/>

                <h1 className="text-center">Tactics Arena Online</h1>

                <form onSubmit={event => this.handleSubmit(event)}>
                    <div className="Login__input-group">
                        <label htmlFor="name">Name</label>
                        <input
                            type="text"
                            id="name"
                            value={this.state.name}
                            onChange={event => this.setState({name: event.target.value})}
                        />
                    </div>
                    <div className="Login__input-group">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            id="password"
                            value={this.state.password}
                            onChange={event => this.setState({password: event.target.value})}
                        />
                    </div>
                    {!this.state.newAccount ? null : (
                        <div className="Login__input-group">
                            <label htmlFor="password_confirm">Repeat Password</label>
                            <input
                                type="password"
                                id="password_confirm"
                                value={this.state.password_confirm}
                                onChange={event => this.setState({password_confirm: event.target.value})}
                            />
                        </div>
                    )}
                    <div className="Login__buttons">
                        <div className="Button" onClick={() => this.toggleNewAccount()} style={{width: '88px'}}>
                            {!this.state.newAccount ? 'New Account' : 'Cancel'}
                        </div>

                        <div className="Button" onClick={() => this.login()}>
                            Login
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default Login;
