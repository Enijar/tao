import React, {Component} from "react";
import PropTypes from "prop-types";
import Container from "../../TAO/Container";
import Messages from "./Messages";
import Players from "./Players";
import MessageBox from "./MessageBox";
import Command from "./Command";
import config from "../../config";

class Chat extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showMessageBox: false,
            messages: config.messages[Container.game.state] || [],
            players: [
                window.data.player,
                {
                    username: 'User B',
                    stats: 750,
                    type: 'grey'
                }
            ]
        };

        this.command = this.command.bind(this);
        this.clearChat = this.clearChat.bind(this);
    }

    componentDidMount() {
        Container.Event.addListener('command', this.command);
        Container.Event.addListener('chat.clear', this.clearChat);
    }

    componentWillUnmount() {
        Container.Event.removeListener('command', this.command);
        Container.Event.removeListener('chat.clear', this.clearChat);
    }

    clearChat() {
        this.setState({messages: []})
    }

    command(command) {
        if (command === 'chat.box') {
            this.toggleMessageBox();
        }
    }

    toggleMessageBox() {
        this.setState({showMessageBox: !this.state.showMessageBox});
    }

    handleMessage(text) {
        if (/^\//.test(text)) {
            const command = new Command({input: text});
            command
                .run()
                .then(({text, type}) => this.addMessage({text, type}))
                .catch(({text, type}) => this.addMessage({text, type}));
            return;
        }

        this.addMessage({text, type: 'player'});
    }

    addMessage({text, type = 'player'}) {
        const {messages} = this.state;
        const message = {type, text};

        if (type === 'player') {
            message.username = window.data.player.username;
        }

        messages.push(message);

        this.setState({messages});
    }

    render() {
        if (this.props.game.state === 'login') {
            return null;
        }
        
        return (
            <div className="Chat">
                <MessageBox show={this.state.showMessageBox} onMessage={message => this.handleMessage(message)}/>

                <div className="Chat__content d-flex justify-content-between" ref="content">
                    <Messages messages={this.state.messages} game={this.props.game}/>
                    <Players players={this.state.players} game={this.props.game}/>
                </div>
            </div>
        );
    }
}

Chat.propTypes = {
    game: PropTypes.object.isRequired,
};

export default Chat;
