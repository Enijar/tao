import React, {Component} from "react";
import PropTypes from "prop-types";
import {Scrollbars} from "react-custom-scrollbars";

class Messages extends Component {
    constructor(props) {
        super(props);

        this.totalMessages = 0;
    }

    getMessage(message) {
        if (message.type === 'player') {
            return `${message.username}: ${message.text}`;
        }

        return message.text;
    }

    componentDidUpdate() {
        if (this.props.messages.length !== this.totalMessages) {
            this.refs.messages.scrollToBottom();
        }

        this.totalMessages = this.props.messages.length;
    }

    render() {
        return (
            <div className="Chat__messages">
                <Scrollbars ref="messages" autoHeight={true}>
                    {this.props.messages.map((message, index) => (
                        <div key={index} className={`Chat__message Chat__message--${message.type}`}>
                            {this.getMessage(message)}
                        </div>
                    ))}
                </Scrollbars>
            </div>
        );
    }
}

Messages.propTypes = {
    messages: PropTypes.array.isRequired,
    game: PropTypes.object.isRequired,
};

Messages.defaultProps = {
    messages: [],
};

export default Messages;
