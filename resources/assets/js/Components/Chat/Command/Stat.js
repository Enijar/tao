class Stat {
    constructor({input}) {
        this.input = input;
    }

    getUsername() {
        return this.input.replace(/^\/stat/, '').trim();
    }

    getStats() {
        // TODO: Fetch stats for requested player's username from DB
        return 750;
    }

    run() {
        return new Promise((resolve, reject) => {
            const username = this.getUsername();
            let message = 'You have';

            if (username.length > 0) {
                message = `${username} has`;
            }

            resolve(`*** ${message} ${this.getStats()} stats. ***`);
        });
    }
}

export default Stat;

