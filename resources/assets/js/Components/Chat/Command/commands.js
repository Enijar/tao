import Stat from "./Stat";
import Help from "./Help";
import Clear from "./Clear";

export default {
    stat: {
        instance: Stat,
        info: [
            {
                command: '/stat',
                description: 'Show your stats.',
            },
            {
                command: '/stat [username]',
                description: 'Show stats for a player.',
            }
        ]
    },
    help: {
        instance: Help,
        info: [
            {
                command: '/help',
                description: 'Show a list of available commands.',
            }
        ]
    },
    clear: {
        instance: Clear,
        info: [
            {
                command: '/clear',
                description: 'Clear the screen of messages.',
            }
        ]
    },
};
