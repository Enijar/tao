import commands from "./commands";

const MAX_CHARS_PER_LINE = 30;

class Help {
    constructor({input}) {
        this.input = input;
    }

    getCommands() {
        const commandList = [];

        for (let command in commands) {
            commandList.push(commands[command]);
        }

        return commandList.map(command => {
            return command.info.map(info => {
                return `${info.command} ${this.getPadding(info)} ${info.description}`;
            }).join("\n");
        }).join("\n");
    }

    getPadding(info) {
        const padding = MAX_CHARS_PER_LINE - (info.command.length);
        return '\xa0'.repeat(padding);
    }

    run() {
        return new Promise((resolve, reject) => {
            resolve(`*** Available commands: ***\n ${this.getCommands()}`);
        });
    }
}

export default Help;
