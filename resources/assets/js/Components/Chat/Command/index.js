import commands from "./commands";

class Command {
    constructor({input}) {
        this.input = input;
        this.commands = commands;
    }

    getName() {
        return this.input.match(/^\/(\w+)/)[1];
    }

    run() {
        return new Promise((resolve, reject) => {
            const name = this.getName();

            if (!this.commands.hasOwnProperty(name)) {
                reject({
                    text: `*** Invalid command "/${name}". Type /help for a list of commands ***`,
                    type: 'command-error'
                });
                return;
            }

            const command = new this.commands[name].instance({input: this.input});

            command
                .run()
                .then(text => {
                    resolve({text, type: 'command-feedback'});
                });
        });
    }
}

export default Command;
