import Container from "../../../TAO/Container";

class Clear {
    constructor({input}) {
        this.input = input;
    }

    run() {
        return new Promise((resolve, reject) => {
            Container.Event.emit('chat.clear');
        });
    }
}

export default Clear;
