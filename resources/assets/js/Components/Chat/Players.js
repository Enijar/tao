import React, {Component} from "react";
import PropTypes from "prop-types";
import {Scrollbars} from "react-custom-scrollbars";

class Players extends Component {
    constructor(props) {
        super(props);

        this.totalPlayers = 0;
    }

    componentDidUpdate() {
        if (this.props.players.length !== this.totalPlayers) {
            this.refs.players.scrollToBottom();
        }

        this.totalPlayers = this.props.players.length;
    }

    getArenaPlayers() {
        if (this.props.game.state !== 'arena') return null;
        return `${this.props.players.length} players in the arena.`;
    }

    getLobbyPlayers() {
        if (this.props.game.state === 'arena') return null;
        return `${this.props.players.length} players in the lobby.`;
    }

    render() {
        return (
            <div className="Chat__players">
                {this.getArenaPlayers()}
                {this.getLobbyPlayers()}

                <Scrollbars ref="players" autoHeight={true}>
                    {this.props.players.map((player, index) => (
                        <div key={index} className="Chat__player">
                            {player.username}
                        </div>
                    ))}
                </Scrollbars>
            </div>
        );
    }
}

Players.propTypes = {
    players: PropTypes.array.isRequired,
    game: PropTypes.object.isRequired,
};

Players.defaultProps = {
    players: [],
};

export default Players;
