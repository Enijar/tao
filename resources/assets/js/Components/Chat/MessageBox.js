import React, {Component} from "react";
import PropTypes from "prop-types";

class MessageBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            message: '',
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        if (this.state.message.trim() === '') {
            return;
        }

        if (this.props.onMessage) {
            this.props.onMessage(this.state.message);
        }

        this.setState({message: ''});
    }

    handleChange(event) {
        this.setState({message: event.target.value});
    }

    render() {
        if (!this.props.show) {
            return null;
        }

        return (
            <form onSubmit={event => this.handleSubmit(event)} className="Chat__message-box">
                <label>Message - Press enter to send</label>
                <input
                    type="text"
                    autoFocus={this.props.autoFocus}
                    value={this.state.message}
                    onChange={event => this.handleChange(event)}
                />
            </form>
        );
    }
}

MessageBox.propTypes = {
    show: PropTypes.bool.isRequired,
    autoFocus: PropTypes.bool.isRequired,
    onMessage: PropTypes.func,
};

MessageBox.defaultProps = {
    show: false,
    autoFocus: true,
};

export default MessageBox;
