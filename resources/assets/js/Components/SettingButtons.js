import React, {Component} from "react";
import PropTypes from "prop-types";
import Container from "../TAO/Container";

class SettingButtons extends Component {
    constructor(props) {
        super(props);
    }

    lobby() {
        Container.game.state = 'lobby';
        Container.Event.emit('game.update');
    }

    settings() {
        // TODO: Add these lines of code back when the settings screen is finished.
        Container.game.state = 'settings';
        Container.Event.emit('game.update');
    }

    exit() {
        // TODO: Add disconnect and cleanup logic here.
        Container.game.state = 'login';
        Container.Event.emit('game.update');
    }

    getLobbyButton() {
        const activeStates = ['arena', 'settings'];
        if (activeStates.indexOf(this.props.game.state) === -1) return null;
        return <div className="SettingButtons__button" onClick={() => this.lobby()}>Lobby</div>;
    }

    getSettingsButton() {
        const activeStates = ['lobby'];
        if (activeStates.indexOf(this.props.game.state) === -1) return null;
        return <div className="SettingButtons__button" onClick={() => this.settings()}>Settings</div>;
    }

    getExitButton() {
        const activeStates = ['lobby', 'settings'];
        if (activeStates.indexOf(this.props.game.state) === -1) return null;
        return <div className="SettingButtons__button" onClick={() => this.exit()}>Exit</div>;
    }

    render() {
        return (
            <div className="SettingButtons">
                {this.getLobbyButton()}
                {this.getSettingsButton()}
                {this.getExitButton()}
            </div>
        );
    }
}

SettingButtons.propTypes = {
    game: PropTypes.object.isRequired,
};

export default SettingButtons;
