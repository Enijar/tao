import React, {Component} from "react";
import PropTypes from "prop-types";
import Container from "../TAO/Container";

class UnitStats extends Component {
    constructor(props) {
        super(props);

        this.state = {
            unit: null,
            selectedUnit: null
        };

        Container.Event.addListener('unit.hover', unit => {
            this.setState({unit});
        });

        Container.Event.addListener('unit.clicked', unit => {
            this.setState({selectedUnit: unit});
        });

        Container.Event.addListener('tile.hover', tile => {
            if (!tile.hasUnit) {
                this.setState({unit: null});
            }
        });

        Container.Event.addListener('tile.clicked', tile => {
            if (!tile.hasUnit) {
                this.setState({selectedUnit: null});
            }
        });
    }

    getUnit() {
        return this.state.unit || this.state.selectedUnit;
    }

    render() {
        if (this.props.game.state !== 'arena') {
            return null;
        }

        const unit = this.getUnit();

        if (unit === null) {
            return null;
        }

        return (
            <div className="UnitStats">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col">
                            <div className="UnitStats__img" style={{backgroundImage: `url(${unit.image})`}}/>
                        </div>

                        <div className="col">
                            <h3>{unit.name}</h3>
                            <h4>Ready!</h4>
                        </div>
                    </div>
                </div>

                <div className="UnitStats__divider"/>

                <div className="container-fluid">
                    <div className="row">
                        <div className="col">
                            <p>
                                Health {unit.health}/{unit.maxHealth}
                            </p>
                        </div>
                        <div className="col">
                            <p>
                                Block {unit.block}%
                            </p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <p>
                                Power {unit.power}
                            </p>
                        </div>
                        <div className="col">
                            <p>
                                Armor {unit.armor}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

UnitStats.propTypes = {
    game: PropTypes.object.isRequired,
};

export default UnitStats;
