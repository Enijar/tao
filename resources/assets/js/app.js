import React from "react";
import {render} from "react-dom";
import Game from "./Components/Game";

render(<Game/>, document.querySelector('#app'));
