import Container from "./index";

// List of keyCodes and their corresponding commands
const Commands = {
    // Enter
    13: 'chat.box',
    // Esc
    27: 'chat.box'
};

document.addEventListener('keyup', event => {
    const keyCode = event.which || event.keyCode;
    const command = Commands[keyCode] || null;

    if (command === null) {
        return;
    }

    Container.Event.emit('command', command);
});

export default Commands;
