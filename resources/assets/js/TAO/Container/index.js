import {EventEmitter} from "fbemitter";
import Commands from "./Commands";

const Container = {
    Event: new EventEmitter(),
    Commands,
    game: {
        state: 'login'
    }
};

export default Container;
