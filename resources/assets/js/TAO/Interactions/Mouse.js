import Container from "../Container";

class Mouse {
    constructor({canvas}) {
        this.canvas = canvas;
        this.pos = {x: 0, y: 0};
        this.clickedPos = {x: 0, y: 0};

        canvas.addEventListener('mousemove', event => {
            this.setPos(event);
            Container.Event.emit('mouse.move', this.pos);
        });

        canvas.addEventListener('mousedown', event => {
            this.setClickedPos(event);
            Container.Event.emit('mouse.click', this.clickedPos);
        });
    }

    getMousePos(x, y) {
        let {top, left} = this.canvas.getBoundingClientRect();
        top = Math.floor(top);
        left = Math.floor(left);

        return {
            x: x - left,
            y: y - top,
        };
    }

    setPos({clientX, clientY}) {
        this.pos = this.getMousePos(clientX, clientY);
    }

    setClickedPos({clientX, clientY}) {
        this.clickedPos = this.getMousePos(clientX, clientY);
    }

    getPos() {
        return this.pos;
    }

    getClickedPos() {
        return this.clickedPos;
    }
}

export default Mouse;
