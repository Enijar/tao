import Container from "../Container";
import config from "../../config";
import {loadImage, rgbaArray} from "../utils";

class Knight {
    constructor({canvas, ctx, mouse, tile}) {
        this.canvas = canvas;
        this.ctx = ctx;
        this.mouse = mouse;
        this.tile = tile;
        this.image = null;
        this.knightCanvas = document.createElement('canvas');
        this.knightCtx = this.knightCanvas.getContext('2d');
        this.width = 52;
        this.height = 80;
        this.x = tile.x1 + (this.width / 2) - 7;
        this.y = tile.y2 - (this.height / 2) + 5;
        this.knightCanvas.width = this.width;
        this.knightCanvas.height = this.height;
        this.stats = {
            move: 3,
            maxHealth: 50,
            health: 50,
            block: 80,
            power: 22,
            armor: 25,
        };
        this.state = {
            showMovableTiles: false,
        };

        Container.Event.addListener('tile.clicked', ({id}) => {
            this.state.showMovableTiles = id === tile.id;

            if (id === tile.id) {
                Container.Event.emit('unit.clicked', this.getUnit());
            }
        });

        Container.Event.addListener('tile.hover', ({id}) => {
            if (id === tile.id) {
                Container.Event.emit('unit.hover', this.getUnit());
            }
        });
    }

    getUnit() {
        return Object.assign({
            name: 'Knight',
            image: this.image
        }, this.stats);
    }

    setImage() {
        if (this.image !== null) {
            return;
        }

        this.knightCanvas.toBlob(blob => {
            this.image = URL.createObjectURL(blob);
            URL.createObjectURL(blob);
        }, 'image/png');
    }

    tintImage() {
        // Apply a tinting color to the bottom image of this unit
        // TODO: Refactor this code to be re-used for all units.

        const knightBottom = loadImage('img/units/knight/front/bottom.png');

        if (knightBottom.width === 0) {
            return;
        }

        this.knightCtx.drawImage(knightBottom, 0, 0, knightBottom.width, knightBottom.height);

        const pixels = this.knightCtx.getImageData(0, 0, knightBottom.width, knightBottom.height);
        const rgba = rgbaArray(config.colors.KNIGHT_COLOR);

        for (let i = 0; i < pixels.data.length; i += 4) {
            if (pixels.data[i + 3] === 0) {
                continue;
            }

            pixels.data[i] = rgba[0];
            pixels.data[i + 1] = rgba[1];
            pixels.data[i + 2] = rgba[2];
            pixels.data[i + 3] = rgba[3] * 255;
        }

        this.knightCtx.globalCompositeOperation = 'multiply';
        this.knightCtx.putImageData(pixels, 0, 0);
        this.knightCtx.drawImage(knightBottom, 0, 0, knightBottom.width, knightBottom.height);

        this.knightCtx.globalCompositeOperation = 'normal';
    }

    showMovableTiles() {
        const {row, index} = this.tile;
        const {grid} = config.game;
        const movableTiles = [];

        // North tiles
        for (let i = 0; i < this.stats.move; i++) {
            // No need to check as there are no movable tiles tiles
            if (row === 0) break;
        }

        // East tiles
        for (let i = 0; i < this.stats.move; i++) {
            // Tile is void, skip
            if (grid[row][index] === 0) continue;

            movableTiles.push({row, index: i});
        }

        // South tiles
        for (let i = 0; i < this.stats.move; i++) {
            // No need to check as there are no movable tiles tiles
            if (row === grid.length - 1) break;
        }

        // West tiles
        for (let i = 0; i < this.stats.move; i++) {
            // No need to check as there are no movable tiles tiles
        }
    }

    render() {
        this.knightCtx.clearRect(0, 0, this.width, this.height);

        this.tintImage();

        const knightMiddle = loadImage('img/units/knight/front/middle.png');
        this.knightCtx.drawImage(knightMiddle, 0, 0, knightMiddle.width, knightMiddle.height);

        const knightTop = loadImage('img/units/knight/front/top.png');
        this.knightCtx.drawImage(knightTop, 0, 0, knightTop.width, knightTop.height);

        this.ctx.drawImage(this.knightCanvas, this.x, this.y, this.width, this.height);

        if (this.state.showMovableTiles) {
            this.showMovableTiles();
        }

        this.setImage();
    }
}

export default Knight;
