import Container from "./Container";
import Arena from "./Components/Arena";
import Lobby from "./Components/Lobby";
import Settings from "./Components/Settings";
import {Mouse} from "./Interactions";

class TAO {
    constructor({canvas, ctx}) {
        this.canvas = canvas;
        this.ctx = ctx;
        this.mouse = new Mouse({canvas});
        this.components = {
            arena: new Arena({canvas, ctx, mouse: this.mouse}),
            lobby: new Lobby({canvas, ctx, mouse: this.mouse}),
            settings: new Settings({canvas, ctx, mouse: this.mouse}),
        };

        this.gameUpdate = this.gameUpdate.bind(this);

        Container.Event.addListener('game.update', this.gameUpdate);

        this.gameUpdate();
    }

    gameUpdate() {
        for (let component in this.components) {
            if (!this.components.hasOwnProperty(component)) {
                continue;
            }

            if (component !== Container.game.state) {
                this.components[component].destroy();
            }

            if (component === Container.game.state) {
                this.components[component].init();
            }
        }
    }

    render() {
        // Request next animation frame and clear canvas
        requestAnimationFrame(() => this.render());
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        // Render components
        for (let component in this.components) {
            if (!this.components.hasOwnProperty(component)) {
                continue;
            }

            if (component === Container.game.state) {
                this.components[component].render();
            }
        }
    }
}

export default TAO;
