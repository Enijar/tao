const loadImage = (src, func) => {
    const img = new Image();
    img.src = src;
    img.onload = () => {
        if (func) {
            func(img);
        }
    };
    return img;
};

const rgbaArray = rgba => {
    return rgba
        .substring(5, rgba.length - 1)
        .replace(/ /g, '')
        .split(',').map(color => parseInt(color));
};

export {loadImage, rgbaArray};
