import Board from "./Board";
import Container from "../Container";

class Lobby {
    constructor({canvas, ctx, mouse}) {
        this.canvas = canvas;
        this.ctx = ctx;
        this.mouse = mouse;
        this.boards = [];
        this.subscriptions = [];

        this.tileClicked = this.tileClicked.bind(this);
    }

    init() {
        this.setTiles();
        this.boards.map(board => board.init());
        this.subscriptions.push(Container.Event.addListener('tile.clicked', this.tileClicked));
    }

    destroy() {
        this.boards.map(board => board.destroy());
        this.subscriptions.map(subscription => subscription.remove());
    }

    tileClicked() {
        Container.game.state = 'arena';
        Container.Event.emit('game.update');
    }

    setTiles() {
        const canvas = this.canvas;
        const ctx = this.ctx;
        const mouse = this.mouse;
        const width = 200;
        const height = 106;
        const spacingX = -20;
        const spacingY = 20;
        const startX = 0;
        const startY = 80;

        for (let i = 0; i < 5; i++) {
            let x = startX + (width * i) + (spacingX * i);

            for (let j = 0; j < 3; j++) {
                let y = startY + (height * j) + (spacingY * j);

                // Don't add last tile.
                if (j === 2 && i === 4) break;

                this.boards.push(new Board({canvas, ctx, mouse, width, height, x, y}));
            }
        }
    }

    render() {
        this.ctx.fillStyle = 'rgba(0, 0, 0, 1)';
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

        this.boards.map(board => board.render());
    }
}

export default Lobby;
