import Board from "./Board";
import Container from "../Container";

class Settings {
    constructor({canvas, ctx, mouse}) {
        this.canvas = canvas;
        this.ctx = ctx;
        this.mouse = mouse;
        this.board = new Board({canvas: this.canvas, ctx: this.ctx, mouse: this.mouse});
        this.subscriptions = [];
    }

    init() {
        this.board.init();
    }

    destroy() {
        this.board.destroy();
        this.subscriptions.map(subscription => subscription.remove());
    }

    render() {
        this.ctx.fillStyle = 'rgba(0, 0, 0, 1)';
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

        this.board.render();
    }
}

export default Settings;
