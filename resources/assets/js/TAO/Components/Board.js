import Container from "../Container";
import config from "../../config";
import {loadImage} from "../utils";

const ORIGINAL_BOARD_WIDTH = 970;
const ORIGINAL_BOARD_HEIGHT = 512;

class Board {
    constructor({canvas, ctx, mouse, width = ORIGINAL_BOARD_WIDTH, height = ORIGINAL_BOARD_HEIGHT, x = 0, y = 0}) {
        this.canvas = canvas;
        this.ctx = ctx;
        this.mouse = mouse;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.board = loadImage('img/board.jpg');
        this.activeTile = {};
        this.subscriptions = [];
        this.scale = this.width / ORIGINAL_BOARD_WIDTH;

        this.mouseClick = this.mouseClick.bind(this);
        this.mouseMove = this.mouseMove.bind(this);
    }

    mouseClick() {
        if (!this.activeTile.hasOwnProperty('id')) return;
        Container.Event.emit('tile.clicked', this.activeTile);
    }

    mouseMove() {
        if (!this.activeTile.hasOwnProperty('id')) {
            this.canvas.style.cursor = 'default';
            return;
        }

        if (Container.game.state === 'lobby') {
            this.canvas.style.cursor = 'pointer';
        }

        Container.Event.emit('tile.hover', this.activeTile);
    }

    init() {
        this.subscriptions.push(Container.Event.addListener('mouse.click', this.mouseClick));
        this.subscriptions.push(Container.Event.addListener('mouse.move', this.mouseMove));
    }

    destroy() {
        this.subscriptions.map(subscription => subscription.remove());
    }

    highlightTiles() {
        // Origin tile coords (top left tile when arena is rotated 45 degrees).
        const x1origin = this.x + (1 * this.scale);
        const y1origin = this.y + (256 * this.scale);
        const x2origin = this.x + (45 * this.scale);
        const y2origin = this.y + (227 * this.scale);
        const x3origin = this.x + (88 * this.scale);
        const y3origin = this.y + (256 * this.scale);
        const x4origin = this.x + (44 * this.scale);
        const y4origin = this.y + (284 * this.scale);

        const border = 1 * this.scale;
        const translateX = x3origin - x1origin + border;
        const translateY = y4origin - y2origin - border;
        let activeTile = false;

        let tiles = config.game.grid;

        if (Container.game.state === 'settings') {
            tiles = [];
            const halfGrid = (config.game.grid.length - 1) / 2;

            for (let i = 0; i < halfGrid; i++) {
                tiles.push(config.game.grid[i]);
            }
        }

        // Draw arena as a grid of tiles.
        tiles.map((tiles, row) => {
            tiles.map((tile, index) => {
                if (tile === 0) {
                    // Tile is void, do nothing.
                    return;
                }

                // The following two variables translate the next tile to the correct position.
                const tX = (translateX * row * .5) + ((translateX * index) / 2);
                const tY = (translateY * row * .5) - ((translateY * index) / 2);

                const tileObj = {
                    x1: x1origin + tX,
                    y1: y1origin + tY,
                    x2: x2origin + tX,
                    y2: y2origin + tY,
                    x3: x3origin + tX,
                    y3: y3origin + tY,
                    x4: x4origin + tX,
                    y4: y4origin + tY,
                    hasUnit: tile > 1,
                    id: `${row}.${index}`,
                    row,
                    index,
                };

                const isActiveTile = this.drawTile(tileObj);

                if (!activeTile && isActiveTile) {
                    activeTile = true;
                }
            });
        });

        if (!activeTile) {
            this.activeTile = {};
        }
    }

    drawTile(tile) {
        const {x1, y1, x2, y2, x3, y3, x4, y4} = tile;
        this.ctx.beginPath();
        this.ctx.moveTo(x1, y1);
        this.ctx.lineTo(x2, y2);
        this.ctx.lineTo(x3, y3);
        this.ctx.lineTo(x4, y4);
        let activeTile = false;

        const {x, y} = this.mouse.getPos();
        if (this.ctx.isPointInPath(x, y)) {
            this.activeTile = tile;
            activeTile = true;
        }

        this.ctx.fillStyle = this.getActiveTileColor(tile);
        this.ctx.fill();
        this.ctx.closePath();

        return activeTile;
    }

    getActiveTileColor(tile) {
        if (!this.activeTile.hasOwnProperty('id')) {
            return config.colors.TILE_COLOR_BLANK;
        }

        if (Container.game.state === 'lobby') {
            const halfGrid = (config.game.grid.length - 1) / 2;
            const topHover = this.activeTile.row < halfGrid && tile.row < halfGrid;
            const bottomHover = this.activeTile.row > halfGrid && tile.row > halfGrid;

            if (topHover) {
                return config.colors.TILE_COLOR_UNIT;
            }

            if (bottomHover) {
                return config.colors.TILE_COLOR_UNIT;
            }
        }

        return config.colors.TILE_COLOR_BLANK;
    }

    render() {
        this.ctx.imageSmoothingEnabled = true;

        if (Container.game.state === 'settings') {
            this.ctx.save();
            this.ctx.beginPath();
            this.ctx.moveTo(0, 0);
            this.ctx.lineTo(704, 0);
            this.ctx.lineTo(704, 90);
            this.ctx.lineTo(43, 512);
            this.ctx.lineTo(43, 0);
            this.ctx.clip();
            this.ctx.drawImage(this.board, this.x, this.y, this.width, this.height);
            this.ctx.closePath();
            this.ctx.restore();
        } else {
            this.ctx.drawImage(this.board, this.x, this.y, this.width, this.height);
        }

        this.highlightTiles();
    }
}

export default Board;
