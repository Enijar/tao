import Container from "../Container";
import config from "../../config";
import {loadImage} from "../utils";
import Knight from "../Units/Knight";

class Arena {
    constructor({canvas, ctx, mouse}) {
        this.canvas = canvas;
        this.ctx = ctx;
        this.mouse = mouse;
        this.board = loadImage('img/board.jpg');
        this.units = [];
        this.activeTile = {};
        this.subscriptions = [];

        this.mouseClick = this.mouseClick.bind(this);
        this.mouseMove = this.mouseMove.bind(this);
    }

    mouseClick() {
        Container.Event.emit('tile.clicked', this.activeTile);
    }

    mouseMove() {
        Container.Event.emit('tile.hover', this.activeTile);
    }

    init() {
        this.subscriptions.push(Container.Event.addListener('mouse.click', this.mouseClick));
        this.subscriptions.push(Container.Event.addListener('mouse.move', this.mouseMove));
    }

    destroy() {
        this.subscriptions.map(subscription => subscription.remove());
    }

    highlightTiles() {
        // Origin tile coords (top left tile when arena is rotated 45 degrees).
        const x1origin = 1;
        const y1origin = 256;
        const x2origin = 45;
        const y2origin = 227;
        const x3origin = 88;
        const y3origin = 256;
        const x4origin = 44;
        const y4origin = 284;

        const border = 1;
        const translateX = x3origin - x1origin + border;
        const translateY = y4origin - y2origin - border;

        // Draw arena as a grid of tiles.
        config.game.grid.map((tiles, row) => {
            tiles.map((tile, index) => {
                if (tile === 0) {
                    // Tile is void, do nothing.
                    return;
                }

                // The following two variables translate the next tile to the correct position.
                const tX = (translateX * row * .5) + ((translateX * index) / 2);
                const tY = (translateY * row * .5) - ((translateY * index) / 2);

                const tileObj = {
                    x1: x1origin + tX,
                    y1: y1origin + tY,
                    x2: x2origin + tX,
                    y2: y2origin + tY,
                    x3: x3origin + tX,
                    y3: y3origin + tY,
                    x4: x4origin + tX,
                    y4: y4origin + tY,
                    hasUnit: tile > 1,
                    id: `${row}.${index}`,
                    row,
                    index,
                };

                this.drawTile(tileObj);
                this.addUnit(tileObj);
            });
        });
    }

    drawTile(tile) {
        const {x1, y1, x2, y2, x3, y3, x4, y4, hasUnit} = tile;
        this.ctx.beginPath();
        this.ctx.moveTo(x1, y1);
        this.ctx.lineTo(x2, y2);
        this.ctx.lineTo(x3, y3);
        this.ctx.lineTo(x4, y4);
        let color = config.colors.TILE_COLOR_BLANK;

        const {x, y} = this.mouse.getPos();
        if (this.ctx.isPointInPath(x, y)) {
            this.activeTile = tile;

            if (hasUnit) {
                color = config.colors.TILE_COLOR_UNIT;
                this.canvas.style.cursor = 'pointer';
            } else {
                this.canvas.style.cursor = 'default';
            }
        }

        this.ctx.fillStyle = color;
        this.ctx.fill();
        this.ctx.closePath();
    }

    addUnit(tile) {
        const {hasUnit} = tile;

        if (!hasUnit) {
            return;
        }

        const unitInstance = this.units.filter(unit => unit.tile.id === tile.id)[0] || null;

        // Add unit instance to the units array.
        if (unitInstance === null) {
            // TODO: Check unit type based on tile number type.
            const knight = new Knight({canvas: this.canvas, ctx: this.ctx, mouse: this.mouse, tile});
            this.units.push(knight);
        }
    }

    render() {
        this.ctx.fillStyle = 'rgba(0, 0, 0, 1)';
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.drawImage(this.board, 0, 0, this.board.width, this.board.height);

        this.highlightTiles();

        this.units.map(unit => unit.render());
    }
}

export default Arena;
