import game from './game';
import colors from './colors';
import messages from './messages';

export default {
    game,
    colors,
    messages,
};
