const defaultMessages = [
    {
        text: '*** Welcome to your FREE Tactics Arena Online account! ***',
        type: 'game'
    },
    {
        text: '*** FREE ACCOUNTS ARE AUTOMATICALLY DISCONNECTED AFTER EACH GAME. ***',
        type: 'game'
    },
    {
        text: '*** Your free account expires in 30 days. ***',
        type: 'game'
    }
];

const messages = {
    lobby: defaultMessages,
    settings: defaultMessages,
    arena: [
        {
            type: 'game',
            text: '*** Entering the arena... ***',
        },
        {
            type: 'game',
            text: '*** User B has entered the arena. ***'
        },
        {
            type: 'game',
            text: '*** You have been randomly picked to make the first move. ***'
        }
    ]
};

export default messages;
