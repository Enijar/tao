const colors = {
    TILE_COLOR_MOVE: 'rgba(0, 132, 255, .4)',
    TILE_COLOR_ATTACK: 'rgba(255, 132, 0, .4)',
    TILE_COLOR_UNIT: 'rgba(255, 255, 255, .4)',
    TILE_COLOR_BLANK: 'rgba(0, 0, 0, 0)',
    KNIGHT_COLOR: 'rgba(0, 127, 249, 1)',
};

export default colors;
