const elixir = require('laravel-elixir');

require('laravel-elixir-browserify-official');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    // Compilation
    mix.sass('app.scss');
    mix.browserify('app.js');

    // Caching
    if (process.env.NODE_ENV === 'production') {
        mix.version(['css/app.css', 'js/app.js'], 'public');
    }
});
