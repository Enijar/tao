# TAO

This is an HTML5 port of the discontinued Flash game [Tactics Arena Online](http://tacticsarenaonline.com/).

### Setup
- Clone this repo
- Run the following commands:
```
composer install # Installs PHP dependencies
npm install # Installs JavaScript dependencies
cp .env.example .env # Copies and created environment variables file
php artisan serve # Optional, runs a server
```
- Visit http://localhost:8000 ()or wherever you access your local projects from)
